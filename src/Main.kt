import java.util.*

val routes = listOf(
        Triple('A', 'B', 6),
        Triple('A', 'D', 7),
        Triple('A', 'C', 6),
        Triple('B', 'A', 6),
        Triple('B', 'D', 5),
        Triple('B', 'E', 10),
        Triple('C', 'A', 6),
        Triple('C', 'D', 8),
        Triple('C', 'F', 7),
        Triple('D', 'A', 7),
        Triple('D', 'B', 5),
        Triple('D', 'C', 8),
        Triple('D', 'E', 6),
        Triple('D', 'F', 11),
        Triple('D', 'G', 9),
        Triple('E', 'B', 10),
        Triple('E', 'D', 6),
        Triple('E', 'G', 8),
        Triple('E', 'H', 7),
        Triple('F', 'C', 7),
        Triple('F', 'D', 11),
        Triple('F', 'G', 10),
        Triple('F', 'H', 9),
        Triple('G', 'D', 9),
        Triple('G', 'E', 8),
        Triple('G', 'F', 10),
        Triple('G', 'H', 5),
        Triple('H', 'E', 7),
        Triple('H', 'F', 9),
        Triple('H', 'G', 5)
)

const val from = 'E'

val minimalDistances = mutableMapOf<Char, MutableMap<Char, Pair<Int, Char>>>()

fun main(args: Array<String>) {
    fillMinimalDistances(routes)
    val verticesWithOrder = routes.groupBy { it.first }.map { it.key to it.value.size }
    val odd = verticesWithOrder.filter { it.second % 2 == 1 }
    val pairings = findPairings(odd.map { it.first })
    val minimum = pairings.map { it to findMinDistance(it.first) + findMinDistance(it.second) }.minBy { it.second }
    val sumOfAllRoads = routes.map { it.third }.sum()
    val totalWeight = sumOfAllRoads / 2 + minimum!!.second
    println("Total cost of optimal route is $totalWeight")
    val path = findPath(from, minimum)
    println("Optimal path from $from is $path")
}

fun findPath(from: Char, minimum: Pair<Pair<Pair<Char, Char>, Pair<Char, Char>>, Int>): MutableList<Char> {
    val routesCopy = routes.toMutableList()
    val newRoute1 = Pair(minimum.first.first.first, minimum.first.first.second)
    val newRoute2 = Pair(minimum.first.second.first, minimum.first.second.second)
    routesCopy.add(Triple(newRoute1.first, newRoute1.second, findMinDistance(newRoute1)))
    routesCopy.add(Triple(newRoute1.second, newRoute1.first, findMinDistance(newRoute1)))
    routesCopy.add(Triple(newRoute2.first, newRoute2.second, findMinDistance(newRoute2)))
    routesCopy.add(Triple(newRoute2.second, newRoute2.first, findMinDistance(newRoute2)))

    val verticesWithOrder = routesCopy.groupBy { it.first }.map { it.key to it.value.size }
    verticesWithOrder.forEach {
        println("count of vertex ${it.first} in the path should be ${it.second/2}")
    }

    println("all vertices count: ${verticesWithOrder.map { it.second }.sum() / 2}")

    val toRemove = mutableListOf<Pair<Char, Char>>()
    val edges = routesCopy.map { it.first to it.second }.toMutableList()
    edges.forEach {
        val reversed = it.second to it.first
        if (!toRemove.contains(it) && edges.contains(reversed)) {
            toRemove.add(reversed)
        }
    }
    edges.removeAll(toRemove)

    val stack = Stack<Char>()
    val path = mutableListOf<Char>()

    var current = from
    while (!stack.isEmpty() || !edges.none { it.first == current || it.second == current }) {
        val none = edges.none { it.first == current || it.second == current }
        if (none) {
            path.add(current)
            current = stack.pop()
        } else {
            for (i in 0 until edges.size) {
                if (edges[i].first == current) {
                    stack.add(current)
                    current = edges[i].second
                    edges.removeAt(i)
                    break
                } else if(edges[i].second == current) {
                    stack.add(current)
                    current = edges[i].first
                    edges.removeAt(i)
                    break
                }
            }
        }
    }

    return path
}

fun findPairings(odd: List<Char>): List<Pair<Pair<Char, Char>, Pair<Char, Char>>> {
    val pairings = mutableListOf<Pair<Pair<Char, Char>, Pair<Char, Char>>>()
    val pairs = generatePairs(odd)
    pairs.forEach { pair ->
        val second = pairs.find { it.first != pair.first && it.first != pair.second && it.second != pair.first && it.second != pair.second }!!
        pairings.add(pair to second)
    }
    return removeDuplicates(pairings)
}

fun removeDuplicates(pairings: MutableList<Pair<Pair<Char, Char>, Pair<Char, Char>>>): List<Pair<Pair<Char, Char>, Pair<Char, Char>>> {
    val pairs = mutableListOf<Pair<Pair<Char, Char>, Pair<Char, Char>>>()
    pairings.forEach {
        if (
                !pairs.contains(it) &&
                !pairs.contains(Pair(it.second, it.first)) &&
                !pairs.contains(Pair(Pair(it.first.second, it.first.first), it.second)) &&
                !pairs.contains(Pair(it.first, Pair(it.second.second, it.second.first))) &&
                !pairs.contains(Pair(Pair(it.first.second, it.first.first), Pair(it.second.second, it.second.first))) &&
                !pairs.contains(Pair(Pair(it.second.second, it.second.first), Pair(it.first.second, it.first.first)))
        ) {
            pairs.add(it)
        }
    }
    return pairs
}

fun generatePairs(odd: List<Char>): List<Pair<Char, Char>> {
    val list = mutableListOf<Pair<Char, Char>>()
    odd.forEach { c ->
        odd.forEach {
            if (c != it) {
                list.add(c to it)
            }
        }
    }
    return list
}

fun findMinDistance(pair: Pair<Char, Char>): Int {
    return minimalDistances[pair.first]!![pair.second]!!.first
}

fun fillMinimalDistances(routes: List<Triple<Char, Char, Int>>) {
    val vertices = routes.map { it.first }.distinct()
    for (vertex in vertices) {
        val visited = mutableListOf<Char>()
        val unvisited = mutableListOf<Char>()
        minimalDistances[vertex] = mutableMapOf()
        vertices.forEach {
            if (it != vertex) {
                minimalDistances[vertex]!![it] = Pair(Int.MAX_VALUE, ' ')
                unvisited.add(it)
            } else {
                minimalDistances[vertex]!![it] = Pair(0, ' ')
            }
        }
        while (visited.size < vertices.size) {
            val distances = minimalDistances[vertex].orEmpty().filter { !visited.contains(it.key) }
            val current = distances.minBy { it.value.first }!!
            routes.filter { it.first == current.key }.forEach {
                val currentDistance = current.value.first + it.third
                if (currentDistance < minimalDistances[vertex]!![it.second]!!.first) {
                    minimalDistances[vertex]!![it.second] = Pair(currentDistance, current.key)
                }
            }
            visited.add(current.key)
            unvisited.remove(current.key)
        }
    }
}